import { Component, OnInit } from '@angular/core';
import { TradeService } from '../trade.service';

@Component({
  selector: 'app-data-centre',
  templateUrl: './data-centre.component.html',
  styleUrls: ['./data-centre.component.css']
})
export class DataCentreComponent implements OnInit {

  public ticker = "C";
  public numDays = 1;
  public priceData: any;
  public chartData: any;

  //class attributes for the chart
  // chart options
  // options
  legend: boolean = true;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Date';
  yAxisLabel: string = 'USD';
  timeline: boolean = true;

  constructor(private tradeService: TradeService) { }

  ngOnInit(): void {
  }

  getPriceData() {
    console.log("---------------")
    this.tradeService.getPrice(this.ticker, this.numDays).subscribe(
      response => {
        console.log("retrieved price data:")
        console.log(response);
        this.priceData = response.price_data;
        this.createChartData();
      },
      error => {
        console.log("Error reading  price data:");
        console.log(error);
      }
    )
  }

  createChartData(){
    //take this.priceData and shape it in the way needed for the chart
    this.chartData = {name: this.ticker, series: []};
    
    for (let price of this.priceData){
      this.chartData.series.push({ name: price[0], 
                                  value: price[1]});
    }

    //wrap it in a list
    this.chartData = [this.chartData];
    console.log(this.chartData);
  }

}
