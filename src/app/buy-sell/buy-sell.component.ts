import { Component, OnInit } from '@angular/core';
import { TradeService } from '../trade.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-buy-sell',
  templateUrl: './buy-sell.component.html',
  styleUrls: ['./buy-sell.component.css']
})
export class BuySellComponent implements OnInit {

  public priceServiceUrl = 'https://tdw7fepw1k.execute-api.eu-west-1.amazonaws.com/default/simplePriceFeed';

  trades: any;
  livePrice: any;

  constructor(private tradeService : TradeService) { }

  ngOnInit(): void {

    this.tradeService.findAll().subscribe( responseData => {
      console.log("Recieved Response Data:")
      console.log(responseData)
     this.trades = responseData
    
    },
    error => {
      console.log("Error retreiving employee list");
      console.log(error);
    }
  );

  }

  async getLivePrice(ticker, numDays) {
    if(typeof(numDays) == 'undefined') {
        numDays = 1;
    }

    const response = await fetch(this.priceServiceUrl + '?ticker=' + ticker + "&num_days=" + numDays);
    return response.json();
}

async  getMostRecentClosePrice(ticker) {
  return (await this.getLivePrice(ticker, 1))['price_data'][0][1]
}


  



     
  public addTrade(form : NgForm) {
    let price = "-1";
    let totalPrice = 0.0;

    this.livePrice = this.getMostRecentClosePrice(form.value.ticker).then(value => {
       price = value

      totalPrice = parseFloat(price) * form.value.quantity;
       let newTrade = JSON.stringify({ticker: form.value.ticker.toUpperCase(), quantity: form.value.quantity, tradeType: form.value.type, price: price, totalPrice});

      
       alert("Total price of trade: $" + totalPrice)

       this.tradeService.addTrade(newTrade).subscribe(
         responseData => {
           console.log("Recieved Response Data:");
           console.log(responseData);
         },
         error => {
           console.log("Error retreiving trade list");
           console.log(error);
         }
       );

       window.location.reload();

     
    });

 


  //this.livePrice = this.getMostRecentClosePrice(form.value.ticker).then(value => console.log(form.value.ticker.toUpperCase() + ": " + value));
  
   // let newTrade = JSON.stringify({ticker: form.value.ticker.toUpperCase(), quantity: form.value.quantity, tradeType: form.value.type});

   // this.tradeService.addTrade(newTrade).subscribe(
     // responseData => {
     //   console.log("Recieved Response Data:");
      //  console.log(responseData);
//},
     // error => {
      //  console.log("Error retreiving trade list");
       // console.log(error);
  //    }
   // );
  } 



}
