import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TradeService {

  constructor(private http : HttpClient) { }


  public findAll() : Observable<any>{
    return this.http.get<any>(
     environment.tradeUrl)
  }

  public addTrade(trade: string): Observable<any> {
    console.log(trade);
    return this.http.post<any>(
    environment.tradeUrl,
    trade,
    {headers : new HttpHeaders({ 'Content-Type': 'application/json' })});
  }

  public getPrice(ticker: string, numDays: number = 1): Observable<any> {
    return this.http.get(
      environment.priceUrl + "?ticker=" + ticker + "&num_days=" + numDays
    )
  }

}





